# EEE3088F_Group35_Design

## Enviro sensing HAT Concept

The HAT in our design project is a Temperature and Light sensor. The HAT can be used to monitor temperature and the presence of light in a controlled environment. The HAT will then use an alarm to signal abnormal changes

## Requirements

    *STM32F0 Discovery Board
    *Putty 
    *STM32 cube IDE
    *18650 Lithium Battery
  


## Collaborators
The project has 3 Collaborators. Francis Mutetwa, Cebile Nkosi and Lithemba Baninzi. 

## License
UCT

